"""
please refer http://www.susa.net/wordpress/2012/06/raspberry-pi-relay-using-gpio/
to configure circuit with relay module


"""
import logging
import RPi.GPIO as GPIO
import time
import xml.dom.minidom

import xml.etree.ElementTree
import datetime
import sqlite3 as lite

from random import randint
import Adafruit_GPIO.SPI as SPI
import Adafruit_MAX31855.MAX31855 as MAX31855
from sparkpost import SparkPost

import schedule


# Raspberry Pi software SPI configuration.
CLK = 25
CS = 24
DO = 18

relay_pin = 17

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
mail_sent = False


# Define a function to convert celsius to fahrenheit.
def c_to_f(c):
    return c * 9.0 / 5.0 + 32.0


def get_param_from_xml(param):
    doc = xml.dom.minidom.parse("config.xml");
    tagName = doc.getElementsByTagName(param)
    if not tagName:
        return None
    else:
        return tagName[0].getAttribute("value")


def get_elaped_hours(elapedtime):
    days, seconds = elapedtime.days, elapedtime.seconds
    hours = days * 24 + seconds / 3600.0
    return hours


def send_mail():
    sp = SparkPost('45b7affdbdbe21ff887127ebdc9c5d8df243061b')

    response = sp.transmissions.send(
        recipients=['wester.de.weerdt@dutchmail.com'],
        html='<p>Hello world</p>',
        from_email='test@sparkpostbox.com',
        subject='Hello from python-sparkpost'
    )

    print(response)


def get_dest_temp(schedule_num, hours):

    if schedule_num < 10:   # built-in schedule
        sch_list = schedule.schedule_list[schedule_num]
    else:
        manual_params = get_param_from_xml('manual_time_interval')
        list_manual = manual_params.split('-')
        sch_list = list_manual[:3]

    print "sch_list :", sch_list
    print "hours : ", hours

    # get number of stage
    cur_stage = 20
    is_final_stage = False

    if schedule_num == 100:   # manual schedule
        hour_acc = 0
        for i in range(len(sch_list)):
            hour_acc += int(sch_list[i])
            if hours < hour_acc:
                cur_stage = i
                break
        print "cur_stage : ", cur_stage

        if cur_stage == 20:  # when finished schedule
            return 0, True

        if cur_stage == 2:
            is_final_stage = True

        if cur_stage > 0:
            hours_in_cur_stage = (hours - hour_acc + int(sch_list[cur_stage])) // 1
        else:
            hours_in_cur_stage = hours
    else:
        for i in range(len(sch_list)):
            if hours < sch_list[i]:
                cur_stage = i
                break

        print "cur_stage : ", cur_stage

        if cur_stage == 20:
            return 0, True

        if cur_stage > 0:
            hours_in_cur_stage = (hours - int(sch_list[cur_stage-1])) // 1
        else:
            hours_in_cur_stage = hours

    print "hours_in_cur_stage : ", hours_in_cur_stage

    temp = 0

    # get temperature via stage/hours
    if schedule_num == 0:   # Novaculits
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 200
        elif cur_stage == 2:
            temp = 200 + 40 * hours_in_cur_stage
        elif cur_stage == 3:
            temp = 540 + 10 * hours_in_cur_stage
        elif cur_stage == 4:
            temp = 850
        elif cur_stage == 5:
            temp = 850 - 10 * hours_in_cur_stage
        elif cur_stage == 6:
            temp = 200
            is_final_stage = True

    elif schedule_num == 1:   # Keokuk
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 200
        elif cur_stage == 2:
            temp = 200 + 40 * hours_in_cur_stage
        elif cur_stage == 3:
            temp = 540 + 10 * hours_in_cur_stage
        elif cur_stage == 4:
            temp = 675
        elif cur_stage == 5:
            temp = 675 - 10 * hours_in_cur_stage
        elif cur_stage == 6:
            temp = 200
            is_final_stage = True

    elif schedule_num == 2:   # Kay County
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 200
        elif cur_stage == 2:
            temp = 200 + 40 * hours_in_cur_stage
        elif cur_stage == 3:
            temp = 650
        elif cur_stage == 4:
            temp = 0
            is_final_stage = True

    elif schedule_num == 3:   # Horse Creek Chert
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 200
        elif cur_stage == 2:
            temp = 800
        elif cur_stage == 3:
            temp = 200
            is_final_stage = True

    elif schedule_num == 4:   # Mookaite Mook Jasper
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 575
        elif cur_stage == 2:
            temp = 0
            is_final_stage = True

    elif schedule_num == 5:   # Withlachoockee Coral GA
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 150 + 50 * hours_in_cur_stage
        elif cur_stage == 2:
            temp = 350
        elif cur_stage == 3:
            temp = 0
            is_final_stage = True

    elif schedule_num == 6:   # Withlachoockee Coral GL
        if cur_stage == 0:
            temp = 150
        elif cur_stage == 1:
            temp = 150 + 50 * hours_in_cur_stage
        elif cur_stage == 2:
            temp = 650
        elif cur_stage == 3:
            temp = 0
            is_final_stage = True

    elif schedule_num == 100:   # manual
        if cur_stage == 0:
            temp = int(list_manual[3])
        elif cur_stage == 1:
            temp = int(list_manual[3]) + int(list_manual[4]) * hours_in_cur_stage
        elif cur_stage == 2:
            init_temp = int(list_manual[3]) + int(list_manual[4]) * int(list_manual[1])
            temp = init_temp - int(list_manual[5]) * hours_in_cur_stage
        elif cur_stage == 3:
            temp = 0
            is_final_stage = True

    print "temp : ", temp

    return temp, is_final_stage


def upload_temp_data(temp):
    conn = None
    db_filename = get_param_from_xml('db_filename')
    try:
        conn = lite.connect(db_filename)
        curs = conn.cursor()
        sql = "CREATE TABLE IF NOT EXISTS temperatures (id INTEGER PRIMARY KEY AUTOINCREMENT," \
              " temp NUMERIC, cur_time TIMESTAMP );"

        curs.execute(sql)
        conn.commit()

        # insert captured temp data
        sql = "INSERT INTO temperatures (temp, cur_time) VALUES(" \
                    + str(temp) + ", datetime('now', 'localtime'));"
        curs.execute(sql)
        conn.commit()

        # keep count of rows as 100
        curs.execute("SELECT count(*) FROM temperatures")
        rows = curs.fetchone()
        row_count = rows[0]

        print "row_count", row_count

        if row_count > 100:
            del_count = row_count - 100
            sql = "Delete from temperatures where id IN" \
                  " (Select id from temperatures limit " + str(del_count) + ");"
            curs.execute(sql)
            conn.commit()

        conn.close()
        return True

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error(" Failed to get captured data")
        return False

    finally:
        if conn:
            conn.close()


def control_relay(temp, is_final_st):

    sensor = MAX31855.MAX31855(CLK, CS, DO)
    tt = sensor.readTempC()
    print "Temp(C) : ", tt
    cur_temp = c_to_f(tt)

#    cur_temp = randint(0, 500)

    print "current temp(F) is ", cur_temp

    completed = False

    upload_temp_data(cur_temp)
    if cur_temp < temp:
        GPIO.output(relay_pin, 1)      # turn on relay
    else:
        GPIO.output(relay_pin, 0)      # turn off relay
        if is_final_st:
            completed = True

    return completed


def send_email(address, content):
    api_key = get_param_from_xml('SparkPost_api_key')

    sp = SparkPost(api_key)

    response = sp.transmissions.send(
        recipients=[address],
        html=content,
        from_email='rock_knapping@sparkpostbox.com',
        subject='Cycle Complete'
    )
    print "Sending complete e-mail"
    print response

    if response:
        return True
    else:
        return False


def main():

    # get params from config file
    cur_schedule = int(get_param_from_xml('current_schedule'))
    str_time = get_param_from_xml('start_time')

    if not str_time:
        return False
    else:
        start_time = datetime.datetime.strptime(str_time, "%Y-%m-%d %H:%M:%S")

    print "started time is ", start_time

    elapsed_hours = get_elaped_hours(datetime.datetime.now() - start_time)
    dest_temp, is_final_st = get_dest_temp(cur_schedule, elapsed_hours)

    print "destination temp is", dest_temp

    ret = control_relay(dest_temp, is_final_st)

    global mail_sent
    if ret:
        if not mail_sent:
            addr = get_param_from_xml('email')
            cont = get_param_from_xml('email_content')
            send_email(addr, cont)
            mail_sent = True
    else:
        mail_sent = False

    return ret

while True:
    time_interval = get_param_from_xml('reading_interval')
    main()
    time.sleep(int(time_interval))
