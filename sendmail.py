from sparkpost import SparkPost

sp = SparkPost('45b7affdbdbe21ff887127ebdc9c5d8df243061b')

response = sp.transmissions.send(
    recipients=['wester.de.weerdt@dutchmail.com'],
    html='<p>Hello world</p>',
    from_email='test@sparkpostbox.com',
    subject='Hello from python-sparkpost'
)

print(response)


