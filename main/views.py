# The MIT License (MIT)
#
# Copyright (c) 2014 Florian Neagu - michaelneagu@gmail.com - https://github.com/k3oni/
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import platform
import os
import multiprocessing
from datetime import timedelta
import logging
import time
import sys
sys.path.append('/opt/rock_knapping/')
import schedule
import xml.dom.minidom
from xml.parsers.expat import ExpatError

import xml.etree.ElementTree
from xml.etree.ElementTree import parse, Element

import datetime
import sqlite3 as lite

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext

from pydash.settings import TIME_JS_REFRESH, TIME_JS_REFRESH_LONG, TIME_JS_REFRESH_NET, VERSION

time_refresh = TIME_JS_REFRESH
time_refresh_long = TIME_JS_REFRESH_LONG
time_refresh_net = TIME_JS_REFRESH_NET
version = VERSION


@login_required(login_url='/login/')
def index(request):
    """

    Index page.

    """
    return HttpResponseRedirect('/main')


def chunks(get, n):
    return [get[i:i + n] for i in range(0, len(get), n)]


def get_uptime():
    """
    Get uptime
    """
    try:
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            uptime_time = str(timedelta(seconds=uptime_seconds))
            data = uptime_time.split('.', 1)[0]

    except Exception as err:
        data = str(err)

    return data


def get_ipaddress():
    """
    Get the IP Address
    """
    data = []
    try:
        eth = os.popen("ip addr | grep LOWER_UP | awk '{print $2}'")
        iface = eth.read().strip().replace(':', '').split('\n')
        eth.close()
        del iface[0]

        for i in iface:
            pipe = os.popen("ip addr show " + i + "| awk '{if ($2 == \"forever\"){!$2} else {print $2}}'")
            data1 = pipe.read().strip().split('\n')
            pipe.close()
            if len(data1) == 2:
                data1.append('unavailable')
            if len(data1) == 3:
                data1.append('unavailable')
            data1[0] = i
            data.append(data1)

        ips = {'interface': iface, 'itfip': data}

        data = ips

    except Exception as err:
        data = str(err)

    return data


def get_cpus():
    """
    Get the number of CPUs and model/type
    """
    try:
        pipe = os.popen("cat /proc/cpuinfo |" + "grep 'model name'")
        data = pipe.read().strip().split(':')[-1]
        pipe.close()

        if not data:
            pipe = os.popen("cat /proc/cpuinfo |" + "grep 'Processor'")
            data = pipe.read().strip().split(':')[-1]
            pipe.close()

        cpus = multiprocessing.cpu_count()

        data = {'cpus': cpus, 'type': data}

    except Exception as err:
        data = str(err)

    return data


def get_users():
    """
    Get the current logged in users
    """
    try:
        pipe = os.popen("who |" + "awk '{print $1, $2, $6}'")
        data = pipe.read().strip().split('\n')
        pipe.close()

        if data == [""]:
            data = None
        else:
            data = [i.split(None, 3) for i in data]

    except Exception as err:
        data = str(err)

    return data


def get_traffic(request):
    return True


def get_platform():
    """
    Get the OS name, hostname and kernel
    """
    try:
        osname = " ".join(platform.linux_distribution())
        uname = platform.uname()

        if osname == '  ':
            osname = uname[0]

        data = {'osname': osname, 'hostname': uname[1], 'kernel': uname[2]}

    except Exception as err:
        data = str(err)

    return data


def get_param_from_xml(param):
    doc = xml.dom.minidom.parse("/opt/rock_knapping/config.xml")
    tagName = doc.getElementsByTagName(param)
    if not tagName:
        return None
    else:
        return tagName[0].getAttribute("value")


def set_param_to_xml(tagname, property_name,  val):
    try:
        doc = xml.dom.minidom.parse("/opt/rock_knapping/config.xml")
        taglist = doc.getElementsByTagName(tagname)

        tag = doc.createElement(tagname)
        tag.setAttribute(property_name, val)

        if taglist:
            doc.firstChild.removeChild(taglist[0])

        doc.firstChild.appendChild(tag)

        file_handle = open("/opt/rock_knapping/config.xml", "wb")
        doc.writexml(file_handle)
        file_handle.close()
        return True

    except ExpatError:
        return False


def get_disk():

    str_time = get_param_from_xml('start_time')
    if not str_time:
        return False
    else:
        start_time = datetime.datetime.strptime(str_time, "%Y-%m-%d %H:%M:%S")

    cur_schedule = int(get_param_from_xml('current_schedule'))
    hour_list = []
    if cur_schedule < 100:
        hour_list = schedule.schedule_list[cur_schedule]
        total_hours = hour_list[-1]
    else:       # manual
        tmp = get_param_from_xml('manual_time_interval')
        man_list = tmp.split('-')
        hour_list = man_list[:3]
        total_hours = int(hour_list[0]) + int(hour_list[1]) + int(hour_list[2])
        hour_list.append(total_hours)   # manual list does not have final hour, so add this

    elapedtime = datetime.datetime.now() - start_time
    days, seconds = elapedtime.days, elapedtime.seconds
    elapsed_min = days * 1440 + seconds // 60

    elaped_hours = elapsed_min / 60

    cur_stage = 0
    if cur_schedule == 100:   # manual schedule
        hour_acc = 0
        for i in range(len(hour_list)):
            hour_acc += int(hour_list[i])
            if elaped_hours < hour_acc:
                cur_stage = i
                break
    else:
        for i in range(len(hour_list)):
            if elaped_hours < hour_list[i]:
                cur_stage = i
                break

    return total_hours, elapsed_min, cur_stage, cur_schedule, schedule.schedule_name_list


def get_disk_rw():
    """
    Get the disk reads and writes
    """
    try:
        pipe = os.popen("cat /proc/partitions | grep -v 'major' | awk '{print $4}'")
        data = pipe.read().strip().split('\n')
        pipe.close()

        rws = []
        for i in data:
            if i.isalpha():
                pipe = os.popen("cat /proc/diskstats | grep -w '" + i + "'|awk '{print $4, $8}'")
                rw = pipe.read().strip().split()
                pipe.close()

                rws.append([i, rw[0], rw[1]])

        if not rws:
            pipe = os.popen("cat /proc/diskstats | grep -w '" + data[0] + "'|awk '{print $4, $8}'")
            rw = pipe.read().strip().split()
            pipe.close()

            rws.append([data[0], rw[0], rw[1]])

        data = rws

    except Exception as err:
        data = str(err)

    return data


def get_mem():
    """
    Get the latest values of sensors
    """
    log_file_name = '/opt/rock_knapping/' + get_param_from_xml('logging')
    logging.basicConfig(level=10, filename=log_file_name, format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    db_name = '/opt/rock_knapping/' + get_param_from_xml('db_filename')

    conn = None
    try:
        data = []
        conn = lite.connect(db_name)
        curs = conn.cursor()
        sql = "select cur_time from temperatures ORDER BY id DESC LIMIT 10;"       # get last 10 timestamps
        curs.execute(sql)
        val_list = curs.fetchall()
        val_time = list(val_list)
        val_time.reverse()

        # remove year
        data_time = []
        for t in val_time:
            tmp = t[0].split(' ')
            data_time.append(tmp[1])

        data.append(data_time)

        sql = "select temp from temperatures ORDER BY id DESC LIMIT 10;"       # get last 10 values
        curs.execute(sql)
        val_list = curs.fetchall()
        tmp = list(val_list)
        tmp.reverse()
        data.append(tmp)

        return data

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error("Getting data from DB failed...")

    finally:
        if conn:
            conn.close()


def get_cpu_usage():
    """
    Get the CPU usage and running processes
    """
    try:
        pipe = os.popen("ps aux --sort -%cpu,-rss")
        data = pipe.read().strip().split('\n')
        pipe.close()

        usage = [i.split(None, 10) for i in data]
        del usage[0]

        total_usage = []

        for element in usage:
            usage_cpu = element[2]
            total_usage.append(usage_cpu)

        total_usage = sum(float(i) for i in total_usage)

        total_free = ((100 * int(get_cpus()['cpus'])) - float(total_usage))

        cpu_used = {'free': total_free, 'used': float(total_usage), 'all': usage}

        data = cpu_used

    except Exception as err:
        data = str(err)

    return data


def get_load():
    """
    Get load average
    """
    try:
        data = os.getloadavg()[0]
    except Exception as err:
        data = str(err)

    return data


def get_netstat():
    """
    Get ports and applications
    """
    try:
        pipe = os.popen(
            "ss -tnp | grep ESTAB | awk '{print $4, $5}'| sed 's/::ffff://g' | awk -F: '{print $1, $2}' "
            "| awk 'NF > 0' | sort -n | uniq -c")
        data = pipe.read().strip().split('\n')
        pipe.close()

        data = [i.split(None, 4) for i in data]

    except Exception as err:
        data = str(err)

    return data


@login_required(login_url='/login/')
def getall(request):
    return render_to_response('main.html', {'time_refresh': time_refresh,
                                            'time_refresh_long': time_refresh_long,
                                            'time_refresh_net': time_refresh_net,
                                            'version': version}, context_instance=RequestContext(request))
